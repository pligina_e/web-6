window.alert("Добро Пожаловать!");
function fun() {
    let s = document.getElementsByName("selectt");
    let select = s[0]; //event.target;
    let radios = document.getElementById("radios");
    let cheks = document.getElementById("checkb");
    window.console.log(select.value);
    if (select.value === "300") {
        radios.style.display = "block";
    } else {
        radios.style.display = "none";
    }
    if (select.value === "400") {
        cheks.style.display = "block";
    } else {
        cheks.style.display = "none";
    }
}
function calc() {
    let type_flower = document.getElementById("type_flower");
    let option1 = document.getElementById("option1");
    let option2 = document.getElementById("option2");
    let rad = document.getElementsByName("rad");
    let count = document.getElementById("count");
    let result = document.getElementById("result");
    if (!document.getElementById("count").value.match(/^\d+$/)) {
        window.alert("В поле 'Количество' должно стоять числовое значение!");
        result.innerHTML = "0";
    } else {
        let price = 0;
        price += parseInt(type_flower.options[type_flower.selectedIndex].value);
        if (type_flower.options[type_flower.selectedIndex].value === "400") {
            price += (
                (option1.checked === true)
                ? parseInt(option1.value)
                : 0
            );
            price += (
                (option2.checked === true)
                ? parseInt(option2.value)
                : 0
            );
        }
        if (type_flower.options[type_flower.selectedIndex].value === "300") {
            price += (
                (rad[0].checked === true)
                ? parseInt(rad[0].value)
                : 0
            );
            price += (
                (rad[1].checked === true)
                ? parseInt(rad[1].value)
                : 0
            );
        }
        price = parseInt(count.value) * price;
        result.innerHTML = price;
    }
}
window.addEventListener("DOMContentLoaded", function () {
    let s = document.getElementsByName("selectt");
    fun();
    calc();
    s[0].addEventListener("change", function () {
        fun();
    });
    let r = document.querySelectorAll(".radios input[type=radio]");
    r.forEach(function (radio) {
        radio.addEventListener("change", function (event) {
            let y = event.target;
            window.console.log(y.value);
        });
    });
});
function click1() {
    let f1 = document.getElementsByName("field1");
    let f2 = document.getElementsByName("field2");
    let r = document.getElementById("res");
    if (!document.getElementById("coins").value.match(/^\d+$/)) {
        window.alert("В поле 'Цена товара' должно стоять числовое значение!");
    } else if (!document.getElementById("kolvo").value.match(/^\d+$/)) {
        window.alert("В поле 'Количество' должно стоять числовое значение!");
    } else {
        r.innerHTML = f1[0].value * f2[0].value;
    }
    return false;
}